# POV (PERSISTENCE OF VISION) – OPEN-SOURCE ARDUINO NANO HARDWARE

### orignal site : https://www.electronics-lab.com/project/pov-persistence-of-vision-open-source-arduino-nano-hardware/

![https://www.electronics-lab.com/wp-content/uploads/2021/10/002-3.jpg](https://www.electronics-lab.com/wp-content/uploads/2021/10/002-3.jpg)

This is an easy and simple hardware to create a POV display. The hardware consists of a Hall sensor, Arduino Nano, 17 x 5mm LEDS of various colors, etc. The Hall Sensor is connected to analog **pin A5** with a pull-up resistor. The circuit works with **3.7V to 5V DC** and a 3.7V battery is ideal to use. The user may use as many LEDS as required. This project can also be used to create many applications such as Bar-Graph display, LED light effects. A large 8.5 mm hole is provided to mount the motor coupling. Refer to the diagram for mounting the hall sensor, motor, and magnet.

POV or persistence of vision refers to the optical illusion that occurs when visual perception of an object does not cease for some time after the rays of light proceeding from it have ceased to enter the eye. The illusion has also been described as “retinal persistence”, “persistence of impressions”, simply “persistence” and other variations.

More Information is available here: [http://electronoobs.com/eng_arduino_tut21.php](http://electronoobs.com/eng_arduino_tut21.php)

### **ARDUINO NANO PINS**

- **Hall Sensor:** Analog Pin A5 with Pull Up resistor, Normally High and goes low when magnet is near to the sensor.
- **LED D2 to D18:** Arduino Pin A4, A3, A2, A1, A0, D13, D2, D3, D4, D5, D6, D7, D8, D9, D10, D11, D12
- **LED D1 :** This LED is optional and not installed, user may assemble this LED if the board is used for other application.

Arduino code is available to test the board. The code makes use of 6 LEDs, refer to the diagram to mount the PCB on the motor shaft, Magnet, and Hall sensor potion.

**Credits:** Original Author of the Arduino Code is **Palak Mehta**.

### **FEATURES**

- Supply 3.7V to 5V DC
- On Board 17X LEDs – Various Colours
- On Board Hall Sensor
- Multiple Mounting Holes for Mounting the battery
- 5MM Mounting Hole for Motor Shaft
- PCB Dimensions 194.95 x 29.37 mm

### **SCHEMATIC**

![https://www.electronics-lab.com/wp-content/uploads/2021/10/SCH-4.jpg](https://www.electronics-lab.com/wp-content/uploads/2021/10/SCH-4.jpg)

### **CONNECTIONS**

![https://www.electronics-lab.com/wp-content/uploads/2021/10/CONNECTIONS-3.jpg](https://www.electronics-lab.com/wp-content/uploads/2021/10/CONNECTIONS-3.jpg)

### **MOUNTING ON MOTOR**

![https://www.electronics-lab.com/wp-content/uploads/2021/10/MOUNTING.jpg](https://www.electronics-lab.com/wp-content/uploads/2021/10/MOUNTING.jpg)

### **PARTS LIST**

| NO | QNTY | REF | DESC | MANUFACTURER | SUPPLIER | SUPPLIER PART NO |
| --- | --- | --- | --- | --- | --- | --- |

|  |  |  |  |  |  |  |
| --- | --- | --- | --- | --- | --- | --- |
| 1 | 1 | CN1 | 4 PIN MALE HEADER PITCH 2.54MM | WURTH | DIGIKEY | 732-5317-ND |
| 2 | 3 | D1,C1,R21 | DNP |  |  |  |
| 3 | 1 | C2 | 47uF/25V | NICHICON | DIGIKEY | 493-15345-3-ND |
| 4 | 1 | C3 | 1uF/10V SMD SIZE 0805 | MURATA/YAGEO | DIGIKEY |  |
| 5 | 2 | D2,D3 | LED-ORANGE 5MM THT | WURTH | DIGIKEY | 732-5018-ND |
| 6 | 2 | D4,D5 | LED-YELLOW 5MM THT | BROADCOM | DIGIKEY | 516-3193-2-ND |
| 7 | 4 | D6,D7,D8,D9 | LED-RED 5MM THT | SUN LED | DIGIKEY | 1497-1031-ND |
| 8 | 8 | D10 to D17 | LED-GREEN 5MM THT | CREE | DIGIKEY | C503B-GCN-CY0C0791-ND |
| 9 | 1 | D18 | LED-BLUE 5MM THT | TT | DIGIKEY | 365-1180-ND |
| 10 | 2 | R1,R20 | 0E SMD SIZE 0805 | MURATA/YAGEO | DIGIKEY |  |
| 11 | 16 | R2 TO R17 | 330E 5% SMD SIZE 0805 | MURATA/YAGEO | DIGIKEY |  |
| 12 | 1 | R18 | 100E 5% SMD SIZE 0805 | MURATA/YAGEO | DIGIKEY |  |
| 13 | 1 | R19 | 10E 5% SMD SIZE 0805 | MURATA/YAGEO | DIGIKEY |  |
| 14 | 1 | R22 | 10K 5% SMD SIZE 0805 | MURATA/YAGEO | DIGIKEY |  |
| 15 | 1 | U1 | ARDUINO NANO | ARDUINO | DIGIKEY | 1050-1001-ND |
| 16 | 1 | U2 | A1102LUA-T OR A3144 | ALLEGRO | DIGIKEY | 620-1003-ND |

### **PHOTOS**

![https://www.electronics-lab.com/wp-content/uploads/2021/10/00AA.jpg](https://www.electronics-lab.com/wp-content/uploads/2021/10/00AA.jpg)

![https://www.electronics-lab.com/wp-content/uploads/2021/10/A14.jpg](https://www.electronics-lab.com/wp-content/uploads/2021/10/A14.jpg)

![https://www.electronics-lab.com/wp-content/uploads/2021/10/007-5.jpg](https://www.electronics-lab.com/wp-content/uploads/2021/10/007-5.jpg)

![https://www.electronics-lab.com/wp-content/uploads/2021/10/008-2.jpg](https://www.electronics-lab.com/wp-content/uploads/2021/10/008-2.jpg)